

document.addEventListener("DOMContentLoaded", function(){

    var btnMenu = document.querySelector('.btn-menu');
    var btnRetour = document.querySelector('.retour a');
    var btnTop = document.querySelector('footer button');
    var menu = document.querySelector('nav ul');

    btnMenu.addEventListener('click',function(e){
        e.preventDefault();
        menu.classList.toggle('open');
    });

    btnRetour.addEventListener('click',function(e){
        e.preventDefault();
        menu.classList.toggle('open');
    });


    //Gestion du ScrollTop
    btnTop.addEventListener('click',function(e){
        e.preventDefault();
        TweenLite.to(window, 2, {scrollTo:0});
    })



});